<?php

namespace MCurl;

class Curl {

	public $threads;
	public $queue;
	public $options = array(
		'returntransfer' => 1,
		'useragent' => 'MCurl (https://bitbucket.org/polonskiy/mcurl)',
	);
	private $active = 0;
	private $curl;

	public function __construct($options = array(), $queue = array(), $threads = 10) {
		$this->curl = curl_multi_init();
		$this->queue = $queue;
		$this->threads = $threads;
		$this->options = $options + $this->options;
	}

	public function exec() {
		if ($data = $this->fetch()) return $data;
		while ($this->queue || $this->active) {
			$this->fillThreads();
			$this->process();
			if ($data = $this->fetch()) return $data;
		}
	}

	private function fillThreads() {
		$add = min($this->threads - $this->active, count($this->queue));
		$requests = array_splice($this->queue, 0, $add);
		foreach ($requests as $r) $this->addHandle($r);
	}

	private function addHandle(array $request) {
		$request = $request + $this->options;
		foreach ($request as $k => $v) {
			$k = strtoupper($k);
			$options[constant("CURLOPT_$k")] = $v;
		}
		curl_setopt_array($handle = curl_init(), $options);
		curl_multi_add_handle($this->curl, $handle);
	}

	private function process() {
		curl_multi_exec($this->curl, $this->active);
		curl_multi_select($this->curl);
	}

	private function fetch() {
		$done = curl_multi_info_read($this->curl);
		if (!$done) return null;
		$handle = $done['handle'];
		$response['error'] = curl_error($handle);
		$response['data'] =  curl_multi_getcontent($handle);
		$response += curl_getinfo($handle);
		$this->delHandle($handle);
		return (object) $response;
	}

	private function delHandle($handle) {
		curl_multi_remove_handle($this->curl, $handle);
		curl_close($handle);
	}

}
